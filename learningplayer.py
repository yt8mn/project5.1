"""
learningplayer.py
"""

import random
import copy
from player import Player
from game import Game

class LearningPlayer(Player):
    """
    Grandmaster of every game!

    Learns to play by favoring moves that lead to wins.
    Defers to random move when there isn't enough information.
    """
    
    def __init__(self, name, mark):
        super().__init__(name, mark)

        # each key in training is a serialized game state,
        # the entry is a [win, draw, lose] count for all
        # training games containing that state.
        self._training = {}
        
    def _game_result(self, winner):
        if not winner:
            return 1 # Draw
        elif winner == self._mark:
            return 0 # Win
        else:
            return 2 # Lose

    def learn_from_game(self, state, winner, players, moves):
        """
        Learns from the game represented by moves (starting from
        initial state with players given), that had result winner.

        The training data for each state seen in the game is updated
        according to the outcome.
        """
        result = self._game_result(winner)
        for rmove in moves:
            for pi in range(len(rmove)):
                pmove = rmove[pi]
                player = players[pi]
                staterep = state.serialize()
                if staterep not in self._training:
                    self._training[staterep] = [0, 0, 0]
                self._training[staterep][result] += 1
                # print("Making move [" + player.mark() + "]: " + str(pmove))
                if pmove:
                    state.make_move(player, pmove)
                # print("State: " + str(state))
            
    def train(self, state, players):
        """
        Trains the player by playing a game with players
        with initial state given by state.
        """
        origstate = copy.deepcopy(state)
        assert len(origstate._previous) == 1
        ttg = Game(players, state)
        (winner, moves) = ttg.play()
        self.learn_from_game(origstate, winner, players, moves)
         
    def turn(self, state):
        moves = state.possible_moves(self)
        if moves:
            bestscore = None
            bestmove = None
            untried_moves = []
            # Try each move, and see how the outcomes are at the resulting state.
            for move in moves:
               newstate = state.copy()
               newstate.make_move(self, move)
               canonical = newstate.serialize()
               if canonical in self._training:
                   outcomes = self._training[canonical]
                   # wins are good, loses are bad (ignore draws)
                   noutcomes = sum(outcomes)
                   # score = (2 * outcomes[0] + outcomes[1]) / noutcomes
                   score = outcomes[0] - 2 * outcomes[2]
                   if not bestmove or score > bestscore:
                       bestmove = move
                       bestscore = score
               else: # unseen position
                   untried_moves.append(move)
            
            if bestmove:
                if score > 20:
                    return bestmove
                elif score > 0 and random.random() > 0.2:
                    return bestmove
                elif untried_moves: 
                    return random.choice(untried_moves)
                else:
                    return bestmove
            else:                               
                return random.choice(untried_moves) 
        else:
            return None
