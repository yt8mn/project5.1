"""
tictactoe.py
"""

from squareboard import SquareBoard, Position
from player import Player
from randomplayer import RandomPlayer
       
class TicTacToeBoard(SquareBoard):
    """
    Board for playing (generalized) Tic-Tac-Toe.
    """

    def __init__(self, N):
        super().__init__(N)

    def possible_moves(self, _player):
        """
        Returns the list of possible moves: all empty squares.
        """
        return self.empty_positions()

    def legal_move(self, player, position):
        return not self.has_object(position)
    
    def make_move(self, player, position):
        assert self.legal_move(player, position)
        self.place_object(position, player.mark())

    def game_over(self):
        """
        The game is over if the board is full, or there is a winner.
        """
        return not self.empty_positions() or self.outcome()
         
    def outcome(self):
        """
        Returns state of game:
            Mark: win for player with that mark
            None: draw

        A player wins the game if they have a line of N of their marks
        (either vertical, horizontal, or diagonal).  If more than one
        player has a winning line, the game is invalid.
        """
        N = self._size
        
        lines = \
          [[Position(x, y) for x in range(N)] for y in range(N)] + \
          [[Position(x, y) for y in range(N)] for x in range(N)] + \
          [[Position(x, x) for x in range(N)]] + \
          [[Position((N - 1) - x, x) for x in range(N)]]

        winner = None
        for line in lines:
            mark = self.get_object(line[0])
            if mark:
                for position in line:
                    if not self.get_object(position) == mark:
                        mark = None
                        break
                if mark:
                    # fails if there are two winners!
                    assert not winner or winner == mark
                    winner = mark
        return winner

def test_tictactoe():
    ttb = TicTacToeBoard(3)
    assert ttb.evaluate() == None
    ttb.place_object(Position(0, 0), "X")
    ttb.place_object(Position(0, 1), "X")
    ttb.place_object(Position(0, 2), "X")
    assert ttb.evaluate() == "X"
    ttb.remove_object(Position(0, 1))
    assert ttb.evaluate() == None
    ttb.place_object(Position(0,1), "O")
    ttb.place_object(Position(1,1), "O")
    ttb.place_object(Position(2,1), "O")
    assert ttb.evaluate() == "O"
    ttb.remove_object(Position(1,1))
    ttb.place_object(Position(1,1), "X")
    # ttb.place_object(Position(0,0), "X")
    ttb.place_object(Position(2,2), "X")
    assert ttb.evaluate() == "X"
    ttb.remove_object(Position(2,2))
    assert ttb.evaluate() == None
    ttb.place_object(Position(2,0), "X")
    assert ttb.evaluate() == "X"
    print("Success!")

